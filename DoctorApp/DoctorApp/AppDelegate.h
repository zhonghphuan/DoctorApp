//
//  AppDelegate.h
//  DoctorApp
//
//  Created by Five on 16/10/31.
//  Copyright © 2016年 Five. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

