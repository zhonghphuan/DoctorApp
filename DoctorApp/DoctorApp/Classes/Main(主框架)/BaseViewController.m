//
//  BaseViewController.m
//  QuickCure1
//
//  Created by maoge on 16/10/30.
//  Copyright © 2016年 maoge. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = WHITE_COLOR;
    self.navigationController.navigationBar.barTintColor = kThemeColor;
    
    [self setUpLeftButtonItem];
}

/*
 *  默认修改了左上角的item值
 */
- (void)setUpLeftButtonItem{
    
    UIButton *leftBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [leftBtn setImage:[UIImage imageNamed:@"jiantou"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
}

/*
 *  如果想任意跳转，就重写goBack方法
 */
- (void)goBack{
    
    [self.navigationController popViewControllerAnimated:YES];
}

//  ***********************【 侧滑 】**********************
/** 中间的nav */
//所有左侧控制器共有方法 定位唯一导航控制器
- (UINavigationController *)centerNavigationController{
    
    return (UINavigationController *)self.sidePanelController.centerPanel;
}

//所有左侧控制器共有方法 切换中心控制器
- (void)jumpToOtherViewController:(UIViewController *)otherVC{
    
    //显示中间的panel
    [self.sidePanelController showCenterPanelAnimated:YES];
    
    //push到指定的VC
    [[self centerNavigationController] pushViewController:otherVC animated:NO];
    //用yes 双重动画 不好看
}

@end
