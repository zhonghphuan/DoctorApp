//
//  BaseViewController.h
//  QuickCure1
//
//  Created by maoge on 16/10/30.
//  Copyright © 2016年 maoge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

//  ***********************【 侧滑 】**********************
/** 中间的指定VC */
- (UINavigationController *)centerNavigationController;

/*
 *  侧滑调转到指定控制器
 */
- (void)jumpToOtherViewController:(UIViewController *)otherVC;
//  ***********************【 侧滑 】**********************


@end
