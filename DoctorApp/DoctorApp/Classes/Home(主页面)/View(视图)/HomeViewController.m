//
//  HomeViewController.m
//  QuickCure1
//
//  Created by maoge on 16/10/30.
//  Copyright © 2016年 maoge. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];

    
    [self setupUI];
}

- (void)setupUI {
        
    self.title = @"黑马快医";

    self.navigationController.navigationBar.barTintColor = kThemeColor;

}




@end
