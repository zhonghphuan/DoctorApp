//
//  Disease.h
//  QuickCure1
//
//  Created by maoge on 16/10/30.
//  Copyright © 2016年 maoge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Disease : NSObject

@property (nonatomic, assign) NSInteger ci3_id;

/** 名称 */
@property (nonatomic, copy) NSString *ci3_name;  //名称

/** 首字母 */
@property (nonatomic, copy) NSString *ci3_name_firstLetter;

/** 拼音 */
@property (nonatomic, copy) NSString *ci3_name_pinyin;


@property (nonatomic, assign) NSInteger ci2_id;


//字典转模型
+ (instancetype)diseaseWithDic:(NSDictionary *)dic;
@end
