//
//  Disease.m
//  QuickCure1
//
//  Created by maoge on 16/10/30.
//  Copyright © 2016年 maoge. All rights reserved.
//

#import "Disease.h"

@implementation Disease

-(instancetype)initWithDictionary:(NSDictionary *)dic{
    
    if (self = [super init]) {
        
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

+ (instancetype)diseaseWithDic:(NSDictionary *)dic{
    
    return [[self alloc] initWithDictionary:dic];
}

@end
