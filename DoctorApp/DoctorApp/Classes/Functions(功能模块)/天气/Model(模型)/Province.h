//
//  Province.h
//  QuickCure1
//
//  Created by maoge on 16/10/30.
//  Copyright © 2016年 maoge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Province : NSObject

@property (nonatomic, strong) NSArray *cities;

@property (nonatomic, copy) NSString *name;


/** 字典转模型 */
+ (instancetype)provinceWithDic:(NSDictionary *)dic;

/** 字典转模型 传进来数组 */
+ (NSArray *)citiesWithArray:(NSArray *)arr;

@end
