//
//  Province.m
//  QuickCure1
//
//  Created by maoge on 16/10/30.
//  Copyright © 2016年 maoge. All rights reserved.
//

#import "Province.h"

@implementation Province

+ (instancetype)provinceWithDic:(NSDictionary *)dic{
    
    Province *province = [[Province alloc] init];
    
    [province setValuesForKeysWithDictionary:dic];
    
    return province;
}


+ (NSArray *)citiesWithArray:(NSArray *)arr{
    
    NSMutableArray *arrM = [NSMutableArray arrayWithCapacity:10];
    
    for (int i = 0; i < arr.count; ++i) {
        
        Province *province =   [Province provinceWithDic:arr[i]];
        
        [arrM addObject:province];
    }
    
    return arrM.copy;
}

@end
