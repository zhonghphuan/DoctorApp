//
//  Weather.h
//  QuickCure1
//
//  Created by maoge on 16/10/30.
//  Copyright © 2016年 maoge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Weather : NSObject

///  天气照片
@property (nonatomic, copy) NSString *dayPictureUrl;

/// 天气 `阴天`
@property (nonatomic, copy) NSString *weather;

///  温度
@property (nonatomic, copy) NSString *temperature;

///  日期
@property (nonatomic, copy) NSString *date;


/** 字典转模型 */
+ (instancetype)weatherWithDic:(NSDictionary *)dic;

@end
/**
 
 NSDictionary *dic = responseObject[@"results"][0][@"weather_data"][0];
 
 successBlock(dic);
 
 
 天气= {
	status = success;
	results = (
	{
	weather_data = (
	{
	nightPictureUrl = http://api.map.baidu.com/images/weather/night/xiaoyu.png;
	weather = 小雨;
	wind = 东北风微风;
	temperature = 26 ~ 22℃;
	dayPictureUrl = http://api.map.baidu.com/images/weather/day/xiaoyu.png;
	date = 周日 09月11日 (实时：21℃);
 }
 ,
	{
	nightPictureUrl = http://api.map.baidu.com/images/weather/night/duoyun.png;
	weather = 多云;
	wind = 东北风微风;
	temperature = 29 ~ 24℃;
	dayPictureUrl = http://api.map.baidu.com/images/weather/day/duoyun.png;
	date = 周一;
 }
 ,
	{
	nightPictureUrl = http://api.map.baidu.com/images/weather/night/xiaoyu.png;
	weather = 多云转小雨;
	wind = 东北风微风;
	temperature = 29 ~ 24℃;
	dayPictureUrl = http://api.map.baidu.com/images/weather/day/duoyun.png;
	date = 周二;
 }
 ,
	{
	nightPictureUrl = http://api.map.baidu.com/images/weather/night/zhenyu.png;
	weather = 小雨转阵雨;
	wind = 东北风微风;
	temperature = 26 ~ 24℃;
	dayPictureUrl = http://api.map.baidu.com/images/weather/day/xiaoyu.png;
	date = 周三;
 }
 ,
 );
	currentCity = 上海;
	pm25 = 63;
	index = (
	{
	title = 穿衣;
	zs = 热;
	tipt = 穿衣指数;
	des = 天气热，建议着短裙、短裤、短薄外套、T恤等夏季服装。;
 }
 ,
	{
	title = 洗车;
	zs = 不宜;
	tipt = 洗车指数;
	des = 不宜洗车，未来24小时内有雨，如果在此期间洗车，雨水和路上的泥水可能会再次弄脏您的爱车。;
 }
 ,
	{
	title = 旅游;
	zs = 适宜;
	tipt = 旅游指数;
	des = 天气较好，但丝毫不会影响您出行的心情。温度适宜又有微风相伴，适宜旅游。;
 }
 ,
	{
	title = 感冒;
	zs = 少发;
	tipt = 感冒指数;
	des = 各项气象条件适宜，无明显降温过程，发生感冒机率较低。;
 }
 ,
	{
	title = 运动;
	zs = 较适宜;
	tipt = 运动指数;
	des = 天气较好，户外运动请注意防晒，推荐您在室内进行低强度运动。;
 }
 ,
	{
	title = 紫外线强度;
	zs = 弱;
	tipt = 紫外线强度指数;
	des = 紫外线强度较弱，建议出门前涂擦SPF在12-15之间、PA+的防晒护肤品。;
 }
 ,
 );
 }
 ,
 );
	error = 0;
	date = 2016-09-11;
 }
 
 */
