//
//  Weather.m
//  QuickCure1
//
//  Created by maoge on 16/10/30.
//  Copyright © 2016年 maoge. All rights reserved.
//

#import "Weather.h"

@implementation Weather

+ (instancetype)weatherWithDic:(NSDictionary *)dic{
    
    Weather *dto =[[Weather alloc] init];
    
    dto.dayPictureUrl = dic[@"dayPictureUrl"];
    dto.weather       = dic[@"weather"];
    dto.temperature   = dic[@"temperature"];
    dto.date          = dic[@"date"];
    
    return dto;
}
@end
