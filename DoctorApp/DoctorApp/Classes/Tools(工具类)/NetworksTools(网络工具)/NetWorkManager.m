//
//  NetWorkManager.m
//  QuickCure1
//
//  Created by maoge on 16/10/30.
//  Copyright © 2016年 haoge. All rights reserved.
//

#import "NetWorkManager.h"
#import "NetWorkTool.h"
#import "Users.h"
#import "Disease.h"


@implementation NetWorkManager

/** 单例 */
+(instancetype)sharedManager{
    
    static NetWorkManager *instance = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        
        instance = [[NetWorkManager alloc] init];
        
    });
    return instance;
}

/** 请求天气 */
- (void)loadWeatherWithCurrentCity:(NSString *)currentCity
                      successBlock:(void(^)(NSDictionary *cityDic))successBlock
                        errorBlock:(void(^)(NSString *error))errorBlock{
    
    
    if ([[NetWorkTool shareManager]isReachableByAFN]) {
        
        NSString *city = currentCity;
        NSString *url = @"http://api.map.baidu.com/telematics/v3/weather";
        
        NSDictionary *paramDic = @{@"location":city,
                                   @"output":@"json",
                                   @"ak":@"17IvzuqmKrK1cGwNL6VQebF9"
                                   };
        
        [[NetWorkTool shareManager] GET_RequestWithUrlString:url parmas:paramDic successBloock:^(id responseObject) {
            
            NSInteger errorCode = [responseObject[@"error"] integerValue];
            
            if (errorCode == 0) {
                
                NSDictionary *dic = responseObject[@"results"][0][@"weather_data"][0];
                
                successBlock(dic);
                
            }else{
                
                NSLog(@"错误");
                
                NSLog(@"没有此城市数据");
            }
            
        } failureBlock:^(NSError *error) {
            
            NSLog(kUnKnownFailureStatus);
            
        }];
        
    }else{
        
        NSLog(kNetWorkFailureStatus);
        
    }
}

/**
 *  肿瘤疾病选择疾病细分列表接口
 */
-(void)loadDiseaseSubWithCi_id:(int)ci_id
                          page:(int)page
                  successBlock:(void(^)(NSArray *dtoArray))successBlock
                    errorBlock:(void(^)(NSString *error))errorBlock{
    
    NSString *urlString = kSearchCI3Url;
    NSDictionary *paramDic = @{
                               @"ci1_id":@(ci_id),
                               @"page_size":@(page),
                               @"page":@1,
                               @"keyword":@""
                               };
    
    if ([[NetWorkTool shareManager] isReachableByAFN]) {
        
        [[NetWorkTool shareManager] POST_RequestWithUrlString:urlString parmas:paramDic successBloock:^(id responseObject) {
            
            NSArray *arr = responseObject[@"data"];
            
            NSMutableArray *tempArray = [NSMutableArray array];
            
            for (NSDictionary *dic in arr) {
                
                Disease *disease = [Disease diseaseWithDic:dic];
                
                [tempArray addObject:disease];
            }
            
            successBlock(tempArray);
            
        } failureBlock:^(NSError *error) {
            
            NSLog(@"error = %@",error);
        }];
        
    }else{
        
        errorBlock(kNetWorkFailureStatus);
    }
}

/*!
 *  登录
 */
- (void)loginUserWihtAccount:(NSString *)account
                    password:(NSString *)password
                successBlock:(void(^)(Users *dto))successBlock
                  errorBlock:(void(^)(NSString *error))errorBlock{
    
            NSString *urlString = kLoginUrl;
    
            NSDictionary *paramDic = @{
                                       @"login_token":@"985c96ce43bb5ef69de3e1ad8afaa83f"
                                       };
    
    
    if ([[NetWorkTool shareManager] isReachableByAFN]) {
        
        
        
        [[NetWorkTool shareManager] POST_RequestWithUrlString:urlString parmas:paramDic successBloock:^(id responseObject) {
            
            NSDictionary *dic =  responseObject[@"data"];
            
            Users *user = [Users userWithDic:dic];
            
            successBlock(user);
            
        } failureBlock:^(NSError *error) {
            
            errorBlock(@"登录失败");
        }];
        
        
    }else{
        
        errorBlock(kNetWorkFailureStatus);
    }
    
}


@end
