//
//  NetWorkManager.h
//  QuickCure1
//
//  Created by maoge on 16/10/30.
//  Copyright © 2016年 haoge. All rights reserved.


// 项目小的话，所有的业务逻辑放在一个类里即可，如果项目大的，要在某一个模块里搞一个业务逻辑层

#import <Foundation/Foundation.h>

@class Users;

@interface NetWorkManager : NSObject

/** 单例 */
+(instancetype)sharedManager;

/** 请求天气 */
- (void)loadWeatherWithCurrentCity:(NSString *)currentCity
                      successBlock:(void(^)(NSDictionary *cityDic))successBlock
                        errorBlock:(void(^)(NSString *error))errorBlock;
/**
 *  肿瘤疾病选择疾病细分列表接口
 */
-(void)loadDiseaseSubWithCi_id:(int)ci_id
                          page:(int)page
                  successBlock:(void(^)(NSArray *dtoArray))successBlock
                    errorBlock:(void(^)(NSString *error))errorBlock;

/*
 *  登录
 */
- (void)loginUserWihtAccount:(NSString *)account
                    password:(NSString *)password
                successBlock:(void(^)(Users *dto))successBlock
                  errorBlock:(void(^)(NSString *error))errorBlock;

@end
