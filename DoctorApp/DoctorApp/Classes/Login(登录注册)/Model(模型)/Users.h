//
//  Users.h
//  QuickCure1
//
//  Created by maoge on 16/10/30.
//  Copyright © 2016年 maoge. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface Users : NSObject
/** 姓名 */
@property (nonatomic, copy) NSString *true_name;

/** 快速创建对象 */
+ (instancetype)userWithDic:(NSDictionary *)dic;
@end


/**
 responseObject = {
 msg = OK;
 data = {
 gender = 1;
 mobile_number = 13269130063;
 province_id = 370000;
 county_id = 0;
 age = 28;
 user_id = 1000089;
 true_name = 王云财;
 weight = 156.0;
 city_id = 0;
 is_certify = 1;
 address = 山东省;
 card_type = 1;
 easymob_password = 123456;
 associate_id = <null>;
 login_token = 321017f8952c7a60fd626c470dd452b9;
 easymob_id = u1000089;
 head_photo = http://hdkj-web1.chinacloudapp.cn:8080/res/1071000089-1451374760656.png;
 card_number = 110109198707061355;
 height = 185.0;
 }
 ;
 code = 0;
 }
 
 
 */
